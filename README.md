# **Image Gallery Project**

## **Install:**
1. Clone project
2. Run `composer install`
3. Run `php app/console doctrine:schema:update --force`
4. Run `php app/console doctrine:fixtures:load`

## **Run tests: **
1. Create parameters.test.yml
1. Create app/phpunit.xml
2. Run `./bin/phpunit -c app`
