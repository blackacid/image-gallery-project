<?php

namespace GalleryBundle\Services;

use GalleryBundle\Entity\Image;
use GalleryBundle\Model\GalleryModel;
use Symfony\Component\Serializer\Serializer;

/**
 * Class GalleryService
 * @package GalleryBundle\Services
 */
class GalleryService
{
    const MAX_IMAGE_SIZE = 10;

    /**
     * @var GalleryModel
     */
    private $galleryModel;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * GalleryService constructor.
     * @param GalleryModel $galleryModel
     */
    public function __construct(GalleryModel $galleryModel, Serializer $serializer)
    {
        $this->galleryModel = $galleryModel;
        $this->serializer = $serializer;
    }

    /**
     * Get N images for each album
     *
     * @return array
     */
    public function getAlbumsWithImagesMaxListArray()
    {
        $results = $this->galleryModel->getImagesWithMaxCount(self::MAX_IMAGE_SIZE);

        $albums = [];

        foreach ($results as $result) {
            /** @var Image $result **/
            $albumId = $result->getAlbum()->getId();
            if (!isset($albums[$albumId])) {
                $albums[$albumId] = [
                    'name' => $result->getAlbum()->getName(),
                    'id' => $albumId,
                    'images' => []
                ];
            }

            $image = $this->serializer->normalize($result, null, ['groups' => ['full']]);

            $albums[$albumId]['images'][] = $image;
        }

        return $albums;
    }

    /**
     * Get album array data
     *
     * @param $album
     * @return array
     */
    public function normalizeAlbum($album)
    {
        return $this->serializer->normalize($album, null, ['groups' => ['album']]);
    }
}
