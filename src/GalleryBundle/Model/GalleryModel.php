<?php

namespace GalleryBundle\Model;

use Doctrine\ORM\EntityManager;
use GalleryBundle\Entity\Album;
use \Doctrine\ORM\QueryBuilder;

/**
 * Class GalleryModel
 * @package GalleryBundle\Model
 */
class GalleryModel
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * GalleryModel constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Get images by album id
     *
     * @param $album
     * @return QueryBuilder
     */
    public function getImagesByAlbumQuery($album)
    {
        $query = $this->em->getRepository('GalleryBundle:Image')->createQueryBuilder('i');

        $query->where('i.album = :album')
            ->setParameter('album', $album->getId());

        return $query;
    }

    /**
     * Get N images for each album
     *
     * @param $maxCount
     * @return array
     */
    public function getImagesWithMaxCount($maxCount)
    {
        $results = $this->em->getRepository('GalleryBundle:Image')->findImagesWithMaxCount($maxCount);

        return $results;
    }
}
