<?php

namespace GalleryBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use GalleryBundle\Entity\Album;
use GalleryBundle\Entity\Image;

/**
 * Class LoadAlbumsData
 * @package GalleryBundle\DataFixtures\ORM
 */
class LoadAlbumsData extends AbstractFixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 5; $i++) {
            $album = new Album();
            $album->setName(sprintf('test%d', $i));

            $imageCount = ($i == 1) ? 5 : 30;

            for ($j = 1; $j <= $imageCount; $j++) {
                $image = new Image();
                $image->setFilename('test.jpg')
                    ->setOriginalFilename(sprintf('originaltest_%d_%d.jpg', $i, $j))
                    ->setAlbum($album);

                $manager->persist($image);
            }
            $manager->persist($album);
        }

        $manager->flush();
    }
}
