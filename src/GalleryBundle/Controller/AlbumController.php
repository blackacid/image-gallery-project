<?php

namespace GalleryBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use GalleryBundle\Entity\Album;

/**
 * Class AlbumController
 * @package GalleryBundle\Controller
 */
class AlbumController extends Controller
{
    const ITEMS_PER_PAGE_COUNT = 10;

    /**
     * Get albums with max 10 images for albums list
     *
     * @return Response
     */
    public function albumsAction()
    {
        $albums = $this->get('gallery.service')->getAlbumsWithImagesMaxListArray();

        return $this->render('@Gallery/list_albums.html.twig', [
            'albums' => $albums
        ]);
    }

    /**
     * Get images by album
     *
     * @ParamConverter("album", options={"id" = "albumId"}, class="GalleryBundle:Album")
     * @param Album $album
     * @param Request $request
     * @return Response
     */
    public function albumAction($album, Request $request)
    {
        $query = $this->get('gallery.model')->getImagesByAlbumQuery($album);
        $page = $request->query->getInt('page', 1);
        $album = $this->get('gallery.service')->normalizeAlbum($album);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $page, self::ITEMS_PER_PAGE_COUNT);

        return $this->render('GalleryBundle::list_images.html.twig', [
            'album' => $album,
            'pagination' => $pagination
        ]);
    }
}
