<?php

namespace GalleryBundle\Repositories;

use Doctrine\ORM\EntityRepository;

/**
 * Class ImageRepository
 * @package GalleryBundle\Repositories
 */
class ImageRepository extends EntityRepository
{
    /**
     * Find images with max count in album
     * 
     * @param $maxCount
     * @return array
     */
    public function findImagesWithMaxCount($maxCount)
    {
        $query = $this->createQueryBuilder('image');
        $query->leftJoin('GalleryBundle:Image', 'im', 'WITH', 'im.album = image.album and image.id < im.id')
            ->groupBy('image.id')
            ->having('COUNT(image.id) < :count')
            ->setParameter('count', $maxCount);

        return $query->getQuery()->getResult();
    }

    /**
     * Get images by album query
     *
     * @param $album
     * @return \Doctrine\ORM\Query
     */
    public function getImagesByAlbumQuery($album)
    {
        $query = $this->createQueryBuilder('i');
        $query->where('i.album = :album')
            ->setParameter('album', $album);

        return $query->getQuery();
    }
}
