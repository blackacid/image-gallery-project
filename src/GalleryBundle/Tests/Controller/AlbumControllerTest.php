<?php

namespace GalleryBundle\Tests\Controller;

use GalleryBundle\Tests\TestCase;

/**
 * Class AlbumControllerTest
 * @package GalleryBundle\Tests\Controller
 */
class AlbumControllerTest extends TestCase
{
    public function testAlbums()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertContains('Albums', $client->getResponse()->getContent());
        $this->assertEquals(5, $crawler->filter('li')->first()->filter('img')->count());
        $this->assertEquals(10, $crawler->filter('li')->last()->filter('img')->count());
    }

    public function testAlbum()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/album/2');

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertContains('test2', $client->getResponse()->getContent());
        $this->assertEquals(10, $crawler->filter('img')->count());
    }
}
