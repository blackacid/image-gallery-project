<?php

namespace GalleryBundle\Tests\Services;

use GalleryBundle\Entity\Album;
use GalleryBundle\Entity\Image;
use GalleryBundle\Model\GalleryModel;
use GalleryBundle\Services\GalleryService;
use Symfony\Component\Serializer\Serializer;

/**
 * Class GalleryServiceTest
 * @package GalleryBundle\Tests\Services
 */
class GalleryServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testGetAlbumsWithImagesMaxListArray()
    {
        $album = $this->createMock(Album::class);
        $album->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(1));

        $image = new Image();
        $image->setAlbum($album)
            ->setFilename('test')
            ->setOriginalFilename('test');

        $serializer = $this
            ->getMockBuilder(Serializer::class)
            ->disableOriginalConstructor()
            ->getMock();

        $serializer->expects($this->once())
            ->method('normalize')
            ->will($this->returnValue(['id' => 1, 'filename' => 'test.jpg']));

        $galleryModel = $this
            ->getMockBuilder(GalleryModel::class)
            ->disableOriginalConstructor()
            ->getMock();
        $galleryModel->expects($this->once())
            ->method('getImagesWithMaxCount')
            ->will($this->returnValue([$image]));

        $galleryService = new GalleryService($galleryModel, $serializer);

        $this->assertNotEmpty($galleryService->getAlbumsWithImagesMaxListArray(10));
    }

    public function testGetAlbumsWithImagesMaxListArrayWithEmptyData()
    {
        $galleryModel = $this
            ->getMockBuilder(GalleryModel::class)
            ->disableOriginalConstructor()
            ->getMock();
        $galleryModel->expects($this->once())
            ->method('getImagesWithMaxCount')
            ->will($this->returnValue([]));

        $galleryService = new GalleryService($galleryModel, new Serializer());

        $this->assertEmpty($galleryService->getAlbumsWithImagesMaxListArray(10));
    }
}
