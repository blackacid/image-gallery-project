<?php

namespace GalleryBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class TestCase
 * @package GalleryBundle\Tests
 */
class TestCase extends WebTestCase
{
    /**
     * @var Application
     */
    protected static $application;

    protected function setUp()
    {
        self::runCommand('doctrine:schema:drop --force --env=test');
        self::runCommand('doctrine:schema:update --force --env=test');
        self::runCommand('doctrine:fixtures:load --env=test');
    }

    /**
     * Run command
     *
     * @param string $command
     * @return int
     */
    protected static function runCommand($command)
    {
        $command = sprintf('%s --quiet', $command);

        return self::getApplication()->run(new StringInput($command));
    }

    /**
     * Get application
     *
     * @return Application
     */
    protected static function getApplication()
    {
        if (null === self::$application) {
            $client = static::createClient();

            self::$application = new Application($client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }
}
