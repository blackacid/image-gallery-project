<?php

namespace GalleryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use GalleryBundle\Doctrine\Traits\TimestampableEntity;
use GalleryBundle\Entity\Album;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Image
 * @package GalleryBundle\Entity
 *
 * @ORM\Table("images")
 * @ORM\Entity(repositoryClass="GalleryBundle\Repositories\ImageRepository")
 */
class Image
{
    use TimestampableEntity;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"full"})
     */
    private $id;

    /**
     * @var Album
     * @ORM\ManyToOne(targetEntity="GalleryBundle\Entity\Album", cascade={"persist"}, inversedBy="images")
     */
    private $album;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Groups({"full"})
     */
    private $filename;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Groups({"full"})
     */
    private $originalFilename;

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get album
     * @return Album
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * Set album
     * @param Album $album
     * @return $this
     */
    public function setAlbum($album)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get file name
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set file name
     *
     * @param string $filename
     * @return $this
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalFilename()
    {
        return $this->originalFilename;
    }

    /**
     * @param string $originalFilename
     * @return $this
     */
    public function setOriginalFilename($originalFilename)
    {
        $this->originalFilename = $originalFilename;

        return $this;
    }
}
