<?php

namespace GalleryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use GalleryBundle\Doctrine\Traits\TimestampableEntity;
use GalleryBundle\Entity\Image;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Album
 * @package GalleryBundle\Entity
 *
 * @ORM\Table("albums")
 * @ORM\Entity
 */
class Album
{
    use TimestampableEntity;

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"full", "album"})
     */
    private $id;

    /**
     *
     * @var string
     * @ORM\Column(type="text")
     * @Groups({"full", "album"})
     */
    private $name;

    /**
     * @var Image[]
     * @ORM\OneToMany(targetEntity="GalleryBundle\Entity\Image", mappedBy="album", cascade={"persist"})
     * @Groups({"full"})
     */
    private $images;

    /**
     * Album constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * Get album name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set album name
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get images
     * @return Image[]
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}
